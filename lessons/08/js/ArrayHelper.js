export default class ArrayHelper {
  static groupBy(array, property) { // property = age, name, country
    const resObject = {};
    array.forEach(obj => {
      function recLookup(obj, path) { /// path = 'work.companyName'
        const properties = path.split('.');
        if (properties.length === 1) {
          return obj[properties[0]];
        }
        return recLookup(obj[properties[0]], properties.slice(1).join('.'))
      }
      const value = recLookup(obj, property);
      if(!resObject.hasOwnProperty(value)) {
        resObject[value] = [obj]
      } else {
        resObject[value].push(obj)
      }
    });
    return resObject;
  }
}
