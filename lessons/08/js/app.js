'use strict';
import ArrayHelper from './ArrayHelper';
import _ from 'lodash'

/*Array.prototype.groupBy = function (property) { // property = age, name, country
  const resObject = {};
  this.forEach(obj => {
    function recLookup(obj, path) { /// path = 'work.companyName'
      const properties = path.split('.');
      if (properties.length === 1) {
        return obj[properties[0]];
      }
      return recLookup(obj[properties[0]], properties.slice(1).join('.'))
    }
    const value = recLookup(obj, property);
    if(!resObject.hasOwnProperty(value)) {
      resObject[value] = [obj]
    } else {
      resObject[value].push(obj)
    }
  });
  return resObject;
}*/

function User (firstName, secondName) {
  this.firstName = firstName;
  this.secondName = secondName;
}

User.prototype.getFullName = function () {
  return `${this.firstName} ${this.secondName}`;
}

const vasya = new User('Vasya', 'Pushkin');

console.log(vasya)
console.log(vasya.getFullName())


function SuperUser(firstName, secondName, thirdName) {
  User.call(this, firstName, secondName);
  this.thirdName = thirdName;
}
SuperUser.prototype = Object.create(User.prototype);
// SuperUser.prototype = User.prototype;
const anton = new SuperUser('Anton', 'Ptushkin', 'Antovich');

console.log(anton)
console.log(anton.getFullName())
console.log(anton.hasOwnProperty('firstName'))
if(anton.hasOwnProperty('firstName')) {
  console.log(anton['firstName'])
}

const usersArray = [
  {
    name: 'Anton',
    age: 12,
    country: 'Ukraine',
    work: {
      company: {
        title: 'Nix'
      },
      salary: 1000
    }
  },
  {
    name: 'Petya',
    age: 31,
    country: 'USA',
    work: {
      company: {
        title: 'Intel'
      },
      salary: 1000
    }
  },
  {
    name: 'Kolya',
    age: 12,
    country: 'Ukraine',
    work: {
      company: {
        title: 'Epam'
      },
      salary: 1000
    }
  },
  {
    name: 'Nikita',
    age: 31,
    country: 'USA',
    work: {
      company: {
        title: 'Intel'
      },
      salary: 1000
    }
  }
];

const usersByAge = ArrayHelper.groupBy(usersArray, 'age');
const usersByAgeLo = _.groupBy(usersArray, 'age');
const usersByCountry = ArrayHelper.groupBy(usersArray, 'country');
const usersByCompany = ArrayHelper.groupBy(usersArray, 'work.company.title');
const usersByCompanyLo = _.groupBy(usersArray, 'work.company.title');
console.log('Lo', usersByAgeLo);
console.log(usersByAge);
console.log(usersByCountry);
console.log('Lo', usersByCompanyLo);
console.log(usersByCompany);



function debounce(func, time) {
  return function () {
    setTimeout(func, time);
  }
}


const lazyAlert = debounce(alert, 4000);

lazyAlert()
