'use strict';

const array = [
  1,
  2,
  3,
  4,
  5
];
// const array1 = new Array();

console.log(array)
console.log(array[1])
array[1] = 45
console.log(array[1])
array[5] = 55;
//array[55] = 55;
console.log(array)
// array.push(34, 56, 78)
array.push(34)
console.log(array.push(67))
// console.log(array.length)
console.log(array)
const lastItem = array.pop()
const firstItem = array.shift()
console.log(lastItem, firstItem)
array.unshift(-1)
console.log(array)

const newArray = array.slice(0, 4)
console.log(array)
console.log(newArray)

array.splice(2, 3, 11, 22, 33)
console.log(array)
newArray.length = 0
console.log(newArray)

for(let index = 0; index < array.length; index++) {
  console.log(`element #${index} value: ${array[index]}`)
}

for(const item of array) {
  console.log(item)
}
for(const key in array) {
  console.log(key)
}


for(let index = 0; index < array.length; index++) {
  array[index] **= 2
}
console.log(array)

const randomArray = [];
const size = 10;

for(let index = 0; index < size; index++) {
  randomArray[index] = parseInt(Math.random() * 100)
}
console.log(randomArray)
for(let num of randomArray) {
  if(num % 2 === 0) {
    console.log(num)
  }
}
for(let a = 2; a < 10; a++) {
  for(let b = 2; b < 10; b++) {
    console.log(a * b) // a=2; b=2 | a=2; b=3
  }
}
const matrix = [
  [1, 2, 3],
  [4, 5, 6],
  [7, 8, 9],
];
console.log(matrix[2][1])
const matrix3D = [
  [
    [1, 2, 3],
    [11, 21, 31]
  ],
  [
    [1, 2, 3],
    [11, 21, 31]
  ],
];
console.log(matrix3D[1][1][1] * matrix[2][1])

const names = ['Vasya', 'Pupkin', 'sad', 'asdas']

const [name, secondName, ...otherArray] = names
console.log(name, secondName, otherArray)
const arr = [...randomArray, ...otherArray];
// const arr = randomArray;

console.log(randomArray)
console.log(arr)
//arr.length = 0;
console.log(randomArray)
console.log(arr)


for(let a = 2; a < 10; a++) {
  if(a === 3) {
    continue
  }
  console.log(a)

}
while (true) {
  const num = parseInt(Math.random() * 100)
  console.log('--', num)
  if(num > 60 && num % 2 === 0) {
    console.log(num)
    break;
  }
}
const mt = [];
for (let row = 0; row < 10; row++) {
  mt[row] = []
  for (let cell = 0; cell < 10; cell++) {
    mt[row][cell] = 0;
  }
}

sayHello()

function sayHello() {
  console.log('Hello!')
}


sayHello()

function sum(...args) {
  let sum = 0;
  for (const n of args) {
    sum += n;
  }
  return sum
}

const s = sum(2, 3, 1)

console.log(s)
