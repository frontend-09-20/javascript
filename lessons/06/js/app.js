(function () {
    //'use strict';

    function factorial(n) {
        console.log(this);
        if(n === 1) {
            return 1;
        }
        return n * factorial(n - 1);
    }

    console.log(factorial(5));

    const user = {
        firstName: 'Vasya',
        secondName: 'Pushkin',
        email: 'Vasya@gmail.com',
        age: 45,
        coins: [12, 2, 4, 56],
        getFullName(...args) {
            console.log(args);
            this.coins.forEach((coin) => {
                console.log(coin * this.age);
            });
            return `${this.firstName} ${this.secondName}`
        },
        'Vasya Pushkin': 'lalalala'
    };
    console.log(user.weight)
    user.weight = 150;
    console.log(user)
    console.log(user.firstName)
    console.log(user.secondName)

    user.firstName = 'Anton';

    console.log(user.firstName)
    console.log(user.secondName)

    user.secondName = 'Ptushkin'
    const propName = 'firstName'
    console.log(user[propName])
    console.log(user.secondName)
    console.log(user['Vasya Pushkin'])
    console.log(user.getFullName())

    const articles = [
        {
            id: 1,
            title: 'Some title 1',
            description: 'Some description 1'
        },
        {
            id: 2,
            title: 'Some title 2',
            description: 'Some description 2'
        },
        {
            id: 3,
            title: 'Some title 3',
            description: 'Some description 3'
        }
    ];
    /*const articleTitles = articles.map(function (article) {
        return article.title;
    });*/
    const articleTitles = articles.map(article => article.title);
    console.log(articles)
    console.log(articleTitles)

    const cart = [
        {
            id: 1,
            title: 'Iphone X',
            quantity: 2,
            price: 1000
        },
        {
            id: 2,
            title: 'Iphone XS',
            quantity: 1,
            price: 1500
        },
        {
            id: 3,
            title: 'Samsung Note 20 Ultra',
            quantity: 3,
            price: 1500
        },
    ];
    cart.weight = 10;
    console.log(cart)

    let loopTotalPrice = 0
    let loopTotalQuantity = 0

    for(const product of cart) {
        loopTotalPrice += product.price * product.quantity
        loopTotalQuantity += product.quantity
    }
    console.log('loopTotalPrice = ', loopTotalPrice)
    console.log('loopTotalQuantity = ', loopTotalQuantity)

    let totalPrice = cart.reduce((totalPrice, product) => totalPrice + product.price * product.quantity, 0)
    let totalQuantity = cart.reduce((totalQuantity, product) => totalQuantity + product.quantity, 0)

    console.log('totalPrice = ', totalPrice)
    console.log('totalQuantity = ', totalQuantity)

    /*
    call
    apply
    bind
     */

    const getFullName = user.getFullName.bind(user);
    const getFullName1 = user.getFullName;
    console.log(getFullName(1))
    console.log(getFullName1.call(user, 2, 3))
    console.log(getFullName1.apply(user, [4, 5]))
    function foo() {
        const args = [].slice.apply(arguments)
        const sum = [].reduce.call(arguments, (sum, arg) => arg + sum, 0)
        console.log(args, sum)
    }

    foo(1, 2, 3, 4)


    setTimeout(() => {
        user.getFullName()
    }, 5000);

    const { firstName, secondName, ...otherUser } = user;
    console.log(firstName, secondName, otherUser);
})();
