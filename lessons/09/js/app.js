'use strict';
function injectDisk(diskNum, callback) {
  console.log(`Inject disk #${diskNum}`);
  setTimeout(() => {
    callback(diskNum);
  }, 1000);
}

function install(diskNum, callback) {
  console.log(`Start install from disk #${diskNum}`);
  setTimeout(() => {
    console.log(`Finished install from disk #${diskNum}`);
    callback(diskNum);
  }, 2000);
}

function main() {
  injectDisk(1, function (diskNum) {
    install(diskNum, function (diskNum) {
      injectDisk(diskNum + 1, function (diskNum) {
        install(diskNum, function (diskNum) {
          injectDisk(diskNum + 1, function (diskNum) {
            install(diskNum, function (diskNum) {
              injectDisk(diskNum + 1, function (diskNum) {
                install(diskNum, function (diskNum) {
                  injectDisk(diskNum + 1, function (diskNum) {
                    install(diskNum, function () {
                      console.log('FINISH!!!');
                    })
                  })
                })
              })
            })
          })
        })
      })
    })
  })
}
// main();


function injectDiskP(diskNum) {
  return new Promise((resolve) => {
    console.log(`Inject disk #${diskNum}`);
    setTimeout(() => {
      resolve(diskNum);
    }, 1000);
  });
}

function installP(diskNum) {
  return new Promise((resolve, reject) => {
    console.log(`Start install from disk #${ diskNum }`);
    if (diskNum === 2) {
      reject('Install error');
      return ;
    }
    setTimeout(() => {
      console.log(`Finished install from disk #${ diskNum }`);
      resolve(diskNum);
    }, 2000);
  });
}

function mainP() {
  injectDiskP(1)
  .then(diskNum => installP(diskNum))
  .then(diskNum => injectDiskP(diskNum + 1))
  .then(diskNum => installP(diskNum))
  .then(diskNum => injectDiskP(diskNum + 1))
  .then(diskNum => installP(diskNum))
  .then(diskNum => injectDiskP(diskNum + 1))
  .then(diskNum => installP(diskNum))
  .then(diskNum => injectDiskP(diskNum + 1))
  .then(diskNum => installP(diskNum))
  .then(diskNum => diskNum * 2)
  .then(res => console.log(res))
  .catch(error => {
    console.error(error);
  })
  .finally(() => console.log('FINISH!!!'))
  console.log('sdsdsd')
}
//mainP();
setTimeout(() => {
  fetch('https://jsonplaceholder.typicode.com/todos')
  .then(response => response.json())
  .then(json => {
    console.log(json)

    document.body.innerHTML += `<ul>${renderTodos(json)}</ul>`
  })
}, 5000);

function renderTodos(todos) {
  return todos.reduce(( html,  todo) => html + `
   <li><strong>${todo.title}:</strong> ${todo.completed ? 'YES' : 'NO'}</li> 
  `, '');
}
