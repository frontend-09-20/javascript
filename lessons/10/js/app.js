import 'regenerator-runtime/runtime'
import * as Handlebars from 'handlebars'

(async () => { // main function
  const listOfTodo = await getTodos();
  console.log(listOfTodo)
  const todoTemplate = Handlebars.compile(document.getElementById('todo-list').innerHTML)
  const todoRes = todoTemplate({ todos: listOfTodo });
  console.log(todoRes)
  document.getElementById('list').innerHTML = todoRes;
  const res = await sum(1, 2);
  console.log(res);
  [1, 2, 3, 4, 5].forEach((id) => {
    console.log(id);
  })
  console.log('forEach -  done!');
  const [ topSale, newProducts ] = await Promise.all([
    getTodoById(1),
    getTodoById(2),
    getTodoById(3),
    getTodoById(4)
  ]);
  console.log(topSale)

  console.log('getting todos  -  done!');
})()

async function getTodos() {
    return (await fetch('https://jsonplaceholder.typicode.com/todos')).json()
}
async function getTodoById(id) {
  return (await fetch(`https://jsonplaceholder.typicode.com/todos/${id}`)).json()
}


async function sum(a, b) {
  return a + b;
}
function summ(a, b) {
  return new Promise((resolve) => {
    resolve(a + b)
  })
}
sum(1, 2).then(res => console.log(res) );

const h1 = document.createElement('h1')
h1.setAttribute('class', 'heading')
//h1.innerHTML = '<em>Hello</em>'
h1.innerText = '<em>Hello</em>'
console.log(h1.innerText)

document.body.append(h1)
