export const BASE_API_URL = 'http://localhost:8080';
export const USERS_API_URL = '/users';
export const NEWS_API_URL = '/news';

export function pow (num) {
  return num ** 2;
}
