'use strict';

import { BASE_API_URL, pow } from './constants'
import User from './classes/user'
import Dog from './classes/Dog'
import Cat from './classes/Cat'
import GhostDog from './classes/GhostDog'
import Animal from './classes/Animal'
// import pow from './constants'

function sumMult({ a, b }) {
  return {
    sum: a + b,
    mul: a * b
  }
}

const { sum, mul } = sumMult({ b: 3, a: 2 });

console.log(sum, mul);

console.log(BASE_API_URL)
console.log(pow(2));


const vasya = new User('Vasya');
const petya = new User('Petya');
console.log(vasya)
console.log(vasya.lastName)
console.log(petya.name)
console.log(petya.age)
petya.lastName = 'Petrov';
console.log(petya.fullName)
petya.age = 18;
console.log(petya.age)

User.sayHello()

const barsik = new Dog('Barsik', 'Husky');
const kss = new Cat('Kss', 'Sphinx');
console.log(barsik)
console.log(kss)
console.log(barsik.isAnimal)
console.log(barsik.isDog)
console.log(barsik.isCat)
console.log(kss.isCat)
console.log(kss.isDog)
console.log(barsik.getPhrase())
console.log(kss.getPhrase())

const gsDog = new GhostDog('Ghost Name', 'Ghost Dog')

console.log(gsDog)
//console.log(barsik.getType())

console.log(gsDog instanceof Cat)


