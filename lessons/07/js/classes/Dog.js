import Animal from './Animal'

export default class Dog extends Animal {
  constructor (name, breed) {
    super('dog');
    this.name = name;
    this.breed = breed;
  }
/*  getType() {
    return this.#type;
  }*/
  getPhrase () {
    return `${super.getPhrase()}woofff`
  }
}
