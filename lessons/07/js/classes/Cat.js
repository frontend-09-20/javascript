import Animal from './Animal'

export default class Cat extends Animal {
  constructor (name, breed) {
    super('cat');
    this.name = name;
    this.breed = breed;
  }
  getPhrase () {
    return `meow`
    //return `${super.getPhrase()}meow`
  }
}
