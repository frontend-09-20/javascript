export default class User {
  lastName = 'Last Name';
  #age = 19;

  constructor (name) {
    this.name = name;
  }

  static sayHello() {
    console.log('Hello!');
  }

  get age() {
    return this.#age;
  }

  set age(value) {
    if (value >= this.#age) {
      this.#age = value;
    } else {
      console.error(`Age should be more ${this.#age}`)
    }
  }

  get fullName() {
    return `${this.lastName} ${this.name}`
  }
}

