import Dog from './Dog'

export default class GhostDog extends Dog {
  opacity = '0.5'
}
