export default class Animal {
  #type = '';
  constructor (type) {
    this.#type = type;
  }
  get isAnimal() {
    return true;
  }
  get isDog() {
    return this.#type === 'dog';
  }
  get isCat () {
    return this.#type === 'cat';
  }

  getPhrase() {
    return `${this.#type} say - `;
  }
}
