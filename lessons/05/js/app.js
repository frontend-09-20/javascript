'use strict';

console.log(sum(1));
// console.log(inc(2)); // error
const inc = function (value, increment = 1) {
    return value + increment;
};
console.log(inc(2));
const inc1 = function inc2 (value, increment = 1) {
    return value + increment;
};

const add = (a, b) => a + b;

const incOne = a => a + 1;

console.log(add(2, 3));
console.log(incOne(2));


function calc(a, b, cbFn) {
    cbFn(a + b);
}

calc(2, 5, function (res) {
    console.log('cbFn: ', res);
});
calc(5, 5, res => console.log('cbFn: ', res));

[1, 2, 3, 4, 5, 6, 7].forEach(function(item, index) {
    console.log(`${index}: ${item}`);
});
[1, 2, 3, 4, 5, 6, 7].forEach((item, index) => console.log(item));

let arr = [1, 2, 3, 4, 5, 6, 7];
arr = arr.map(item => item ** 2);
console.log(arr);
function isEven(value) {
    return value % 2 === 0;
}
const filteredArray = arr.filter(isEven);
console.log(filteredArray);

function filter(array, callbackFn) {
    const resultArray = [];
    for(const item of array) {
        if(callbackFn(item)) {
            resultArray.push(item);
        }
    }
    return resultArray;
}
function map(array, callbackFn) {
    const resultArray = [];
    for(const item of array) {
       resultArray.push(callbackFn(item));
    }
    return resultArray;
}
let arr1 = [1, 2, 3, 4, 5, 6, 7];
arr1 = map(arr1, item => item ** 3);
console.log(arr1);
const myFilteredArray = filter(arr, isEven);

console.log(myFilteredArray);
const sumArray = arr1.reduce((sum, item) => sum + item, 0);
console.log(sumArray);

function foo(a) {
    // a = a === 0 ? a : a || 4
    const c = 2;
    return function (b) {
        return b + a * c;
    }
}
const bar = foo(1)
console.log(bar(2));
console.log(bar(9));

function sum(a, b = 3, c = 2) {
    return a + b + c;
}